require('dotenv').config()

import express, { Application, Request, Response, NextFunction } from 'express'
import { json, urlencoded } from 'body-parser'
import helmet from 'helmet'
import morgan from 'morgan'

import { IndexRoute } from './routes'

/**
 * @class App
 * Configuration NodeJS App
 * App Use, Routes, Error Handler, Middleware, GraphQL, etc
 */

class App {
    public app: Application
    private IndexRoute: IndexRoute = new IndexRoute()

    constructor() {
        this.app = express()
        this.config()
        this.routes()
        this.errorHandler()
    }

    /**
     * @function config
     * Config module setting
     */

    private config = (): void => {
        this.app.use(json())
        this.app.use(urlencoded({ extended: false }))
        this.app.use(helmet())
        this.app.use(morgan('dev'))
    }

    /**
     * @function routes
     * Index route, important not to edit
     */

    private routes = (): void => {
        this.IndexRoute.routes(this.app)
    }

    /**
     * @function errorHandler
     * Throw error with next express
     * 
     * @example
     * return next('Your Error Message')
     */

    private errorHandler = (): void => {
        this.app.use((req: Request, res: Response, next: NextFunction): void => {
            const err: any = new Error('Whooppss...!')
            err.status = 404

            next(err)
        })

        if (process.env.NODE_ENV === 'development') {
            this.app.use((err: any, req: Request, res: Response, next: NextFunction): object => { // development mode
                return res.status(err.status || 500).json({
                    status: false,
                    message: err.message,
                    errors: err
                })
            })
        } else {
            this.app.use((err: any, req: Request, res: Response, next: NextFunction): object => { // production mode
                return res.status(err.status || 500).json({
                    status: false,
                    message: err.message
                })
            })
        }
    }
}

export default new App().app