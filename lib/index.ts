require('dotenv').config()

import { createServer, Server as HTTPServer } from 'http'

import { Socket } from './socket/Socket'
import app from './App'

/**
 * @class Server
 * Start all Server
 */

class Server {
    private PORT = process.env.PORT || 3000
    private server: HTTPServer = createServer(app)
    
    constructor() {
        this.startNodeServer()
        this.startSocketServer()
    }

    private startNodeServer = (): void => {
        this.server.listen(this.PORT, () => {
            console.log(`Server running on 127.0.0.1:${this.PORT}`)
        })
    }

    private startSocketServer = (): void => {
        if (process.env.SOCKET_ON === 'True') {
            new Socket(this.server)
        }
    }
}

new Server()