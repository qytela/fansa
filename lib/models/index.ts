import { UserModel } from '../resources/interfaces/models/User.model.interface'

const models = require('../../databases/models')

export const User: UserModel = models.users