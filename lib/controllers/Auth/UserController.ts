require('dotenv').config()

import { Request, Response, NextFunction } from 'express'
import { sign } from 'jsonwebtoken'

import { UserModel } from '../../resources/interfaces/models/User.model.interface'

import { User } from '../../models'

/**
 * @class UserController
 */

export class UserController {
    public index = (req: Request, res: Response): object => {
        return res.json({
            status: true,
            message: 'Hallo world',
            data: req.dataUser
        })
    }

    /**
     * @function login
     * Login account 
     */
    
    public login = async (req: Request, res: Response, next: NextFunction): Promise<object | void> => {
        try {
            const { email, password } = req.body

            const auth = <UserModel> await User.findOne({ where: { email, password }})
            if (!auth) {
                return res.status(401).json({
                    status: false,
                    message: 'Bad authentication'
                })
            }

            const token: string = sign({ user: { id: auth.id } }, process.env.JWT_SECRET)
            return res.json({
                status: true,
                token
            })    
        } catch (error) {
            return next(error)
        }
    }

    /**
     * @function register
     * Register account 
     */

    public register = async (req: Request, res: Response, next: NextFunction): Promise<object | void> => {
        try {
            const { email, password } = req.body

            await User.create({ email, password })

            return res.json({
                status: true,
                message: 'Registration successfully'
            })
        } catch (error) {
            return next(error)
        }
    }
}