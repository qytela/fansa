require('dotenv').config()

import { Server } from 'http'
import { Socket as SocketIO } from 'socket.io'

import { ChannelOneQueue } from './ChannelOneQueue'

/**
 * @class Socket
 * Configuration SocketIO
 */

export class Socket {
    private io: SocketIO
    private ChannelOneQueue: ChannelOneQueue = new ChannelOneQueue()

    constructor(private server: Server) {
        /**
         * Setting SocketIO
         */

        this.io = require('socket.io')(this.server, {
            origin: process.env.SOCKET_ORIGIN,
            pingTimeout: parseInt(process.env.SOCKET_PING_TIMEOUT)
        })

        this.startSocketServer()
    }

    /**
     * @function startSocketServer
     * Start SocketIO Server
     */

    private startSocketServer = (): void => {
        this.io.on('connection', (socket: SocketIO) => {
            console.log('Connect!')

            socket.on('channelOne', (data) => {
                console.log('- Received data', data)
                
                socket.emit('channelTwo', { message: 'Good Job!' })
            })

            socket.on('channelOneQueue', (data) => {
                this.ChannelOneQueue.channelOneQueue(data, socket)
            })
        })
    }
}