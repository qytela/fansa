require('dotenv').config()

import { Socket } from 'socket.io'
import { connect } from 'amqplib/callback_api'

/**
 * @class ChannelOneQueue
 * Configuration RabbitMQ or Message Broker
 */

export class ChannelOneQueue {
    /**
     *  @function channelOneQueue
     * Testing Channel One as Sender
     */

    public channelOneQueue = (data: object, socket: Socket): void => {
        connect(process.env.AMQP_URL, (error, connection) => {
            if (error) throw new Error(error)

            connection.createChannel((error, channel) => {
                if (error) throw new Error(error)

                channel.assertQueue('channelOneQueue', {
                    durable: true
                })

                channel.sendToQueue('channelOneQueue', Buffer.from(JSON.stringify(data)))

                this.channelTwoQueue(socket)
            })

            setTimeout(() => {
                connection.close()
            }, parseInt(process.env.AQMP_TIMEOUT))
        })
    }

    /**
     * @function channelTwoQueue
     * Testing Channel Two as Receiver
     */

    public channelTwoQueue = (socket: Socket): void => {
        connect(process.env.AMQP_URL, (error, connection) => {
            if (error) throw new Error(error)

            connection.createChannel((error, channel) => {
                if (error) throw new Error(error)

                channel.assertQueue('channelOneQueue', {
                    durable: true
                })

                channel.consume('channelOneQueue', (data: any) => {
                    const parseData: object = JSON.parse(data.content)

                    console.log('- Received data', parseData)

                    socket.broadcast.emit('channelTwo', { message: 'Good Job with Queue!' })
                }, {
                    noAck: true
                })
            })
        })
    }
}