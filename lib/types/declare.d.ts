declare namespace Express {
    export interface Request {
        dataUser: {
            id: number
        }
    }
}

type CustomBoolean = 'True' | 'False'

declare namespace NodeJS {
    export interface ProcessEnv {
        NODE_ENV: 'development' | 'test' | 'production'
        JWT_SECRET: string

        DATABASE_ON: CustomBoolean
        
        SOCKET_ON: CustomBoolean
        SOCKET_ORIGIN: string
        SOCKET_PING_TIMEOUT: string

        AMQP_URL: string
        AQMP_TIMEOUT: string
    }
}