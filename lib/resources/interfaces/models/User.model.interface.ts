import { Model } from './Index'

export interface UserModel extends Model<UserModel, any> {
    id: number
    email: string
    password: string
    createdAt?: Date
    updatedAt?: Date
}