require('dotenv').config()

import { Request, Response, NextFunction } from 'express'
import { verify } from 'jsonwebtoken'

import { Verify } from '../resources/interfaces/Jwt.interface'

import { User } from '../models'

/**
 * @class CheckJWT
 * Middleware for Authorization with JsonWebToken
 * Authorization: Bearer <Token>
 */

export class CheckJWT {
    /**
     * @function check
     * Check and Verify Bearer Token
     */

    public check = async (req: Request, res: Response, next: NextFunction): Promise<object | void> => {
        try {
            const headers = <string> req.headers['authorization']
            if (!headers) {
                return res.sendStatus(400)
            }

            const bearerHeader: string[] = headers.split(' ')
            if (bearerHeader[0] !== 'Bearer') {
                return res.sendStatus(400)
            }

            const verifyJwt = <Verify> verify(bearerHeader[1], process.env.JWT_SECRET)
            if (!verifyJwt) {
                return res.sendStatus(401)
            }
            
            const user = await User.findByPk(verifyJwt.user.id)
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'No user found'
                })
            }

            req.dataUser = user
            return next()
        } catch (error) {
            if (error.name === 'JsonWebTokenError') {
                return res.status(400).json({
                    status: false,
                    message: 'Token invalid'
                })
            }
            
            return next(error)
        }
    }
}