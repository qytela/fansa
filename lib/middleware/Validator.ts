import { Request, Response, NextFunction } from 'express'
import { check, validationResult } from 'express-validator'

/**
 * Add your method here with delimeter |
 */

type Method = 'LOGIN' | 'REGISTER'

/**
 * @class Validator
 * Make validate for request
 */

export class Validator {
    /**
     * @function make
     * Add custom validate here
     * more method or example to validate -> (https://express-validator.github.io/docs/)
     */

    public make = (method: Method): any => {
        switch(method) {
            case 'LOGIN':
                return [
                    check('email').not().isEmpty().withMessage('Email is required').isEmail().withMessage('Email must be valid'),
                    check('password').not().isEmpty().withMessage('Password is required'),

                    this.response
                ]
            case 'REGISTER':
                return [
                    check('email').not().isEmpty().withMessage('Email is required').isEmail().withMessage('Email must be valid'),
                    check('password').not().isEmpty().withMessage('Password is required'),
                    check('c_password').not().isEmpty().withMessage('Confirm password is required'),

                    this.response
                ]
        }
    }

    /**
     * @function response
     * Response for validator
     */

    public response = (req: Request, res: Response, next: NextFunction): object | void => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({
                status: false,
                errors: errors.array()
            })
        }

        return next()
    }
}