import { Request, Response, NextFunction } from 'express'

import { UserModel } from '../resources/interfaces/models/User.model.interface'

import { User } from '../models'

/**
 * @class ModelValidator
 * Validate request or checking
 * (ex: email address exists or not)
 * before going to controller
 */

export class ModelValidator {
    /**
     * @function registerValidate
     * Register validation request
     */

    public registerValidate = async (req: Request, res: Response, next: NextFunction): Promise<object | void> => {
        try {
            const { email, password, c_password } = req.body

            if (password !== c_password) {
                return res.status(401).json({
                    status: false,
                    message: 'Password do not match'
                })
            }

            const checkEmail = <UserModel> await User.findOne({ where: { email }})

            if (checkEmail) {
                return res.status(422).json({
                    status: false,
                    message: 'Email address already exists'
                })
            } else {
                return next()
            }
        } catch (error) {
            return next(error)
        }
    }
}