import { Application } from 'express'

export class IndexRoute {
    public routes = (app: Application): void => {
        UserRoute.routes(app)
    }
}

/**
 * Do not edit this
 * This should be below
 */

import UserRoute from './User'