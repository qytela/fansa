import { Application } from 'express'

import { UserController } from '../controllers'
import { CheckJWT, ModelValidator, Validator } from '../middleware'

/**
 * @class UserRoute
 * Configuration Router
 */

class UserRoute {
    private api = '/api/v1'
    private prefix = `${this.api}/users`
    private UserController: UserController = new UserController()
    private CheckJWT: CheckJWT = new CheckJWT()
    private ModelValidator: ModelValidator = new ModelValidator()
    private Validator: Validator = new Validator()

    /**
     * @function routes
     * Setting router for accesing
     */

    public routes = (app: Application): void => {
        app
            .post( // method
                `${this.prefix}/login`, // route
                [this.Validator.make('LOGIN')], // middleware
                this.UserController.login // controller
            )

        app
            .post(
                `${this.prefix}/register`,
                [this.Validator.make('REGISTER'), this.ModelValidator.registerValidate],
                this.UserController.register
            )

        app
            .get(
                `${this.prefix}/test`,
                this.UserController.index
            )

        app
            .get(
                `${this.prefix}`,
                [this.CheckJWT.check],
                this.UserController.index
            )
    }
}

export default new UserRoute()