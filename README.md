# Fansa (TypeScript For Building RestAPI)

![N](https://sdtimes.com/wp-content/uploads/2019/08/typescriptfeature-490x305.png)

Make your RestAPI with TypeScript

- Simple
- Latest all dependencies
- OOP (Object Oriented Programming)
- Without npm init -y

# Includes

- ORM ([Sequelize](https://sequelize.org/))
- Database (Mysql)
- Message Broker ([Rabbitmq](https://www.rabbitmq.com/))
- Websocket ([Socket.io](https://socket.io/))
- Authorization (Jsonwebtoken)
- Environment
- Custom Validator
- Cors
- Command for make (route | controller | model)

# Before Cloning

- Install TypeScript `npm install typescript -g`
- [NodeJS](https://nodejs.org/en/)

# Installation

Clone this repository and go to your project folder
```git
git clone https://gitlab.com/qytela/fansa.git your-project
cd your-project
```

Installing dependencies and dev-dependencies
```npm
npm install && npm run install:dev
```

Compile and run in your browser
```npm
npm run dev
```

# Setting up Database

Before setting, make sure your already install sequelize-cli `npm install sequelize-cli -g`

Copy environment file
```sh
cp .env.example .env
```

Open file `.env` in your root project folder, change `DATABASE_ON=False` to `DATABASE_ON=True`

Go to folder `~/your-project/databases/config` and find file `config.json`, and edit your config database
```json
"development": {
    "username": "YOUR_DATABASE_USERNAME",
    "password": "YOUR_DATABASE_PASSWORD",
    "database": "YOUR_DATABASE_NAME",
    "host": "127.0.0.1",
    "dialect": "mysql"
}
```

For more detail `https://sequelize.org/master/manual/migrations.html`

After edit config file, runing migration with sequelize-cli
```sh
sequelize db:migrate
```

More command for sequelize-cli
```sh
sequelize --help
```

# Testing
- BASE_PATH `api/v1, *ex: http://localhost:3000/api/v1`
- Login `http://localhost:3000/api/v1/users/login`
- Register `http://localhost:3000/api/v1/users/register`

Login or Register body (Application/json)
```json
{
    "email": "test@gmail.com",
    "password": "test"
}
```

# Command

Make new Controller
```node
node cmd make:controller --name AdminController --with-promise yes
```

`--name (Controller name)`
`--with-promise (Default is no)`

Make new Route
```node
node cmd make:route --name Admin --prefix admins --with-promise yes
```

`--name (Route name)`
`--prefix (Prefix route, *ex: http://localhost:3000/api/v1/admins)`
`--with-promise (Default is no)`

Append model, after you make model with sequelize-cli, important to append model with this command

Example make model with sequelize-cli
```sh
sequelize model:generate --name Admin --attributes fullname:string,address:string
```

After make model with sequelize-cli, table name is `Admin`, go to folder and find file `~/your-project/databases/models/admin.js`
```sh
sequelize.define('Admin', {})
```

Append model
```node
node cmd append:model --name Admin --table Admin
```

`--name (Name model)`
`--table (Table model)`

License
----

MIT

**Free Software, Hell Yeah!**