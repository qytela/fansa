import { Request, Response } from 'express'

/**
 * @class SampleController
 */

export class SampleController {
    public index = (req: Request, res: Response): object => {
        return res.json({
            status: true,
            message: 'Hallo world'
        })
    }
}