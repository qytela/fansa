const program = require('commander')
const fs = require('fs')

program
    .command('make:controller')
    .description('Make a new controller')
    .option('--name <name>', 'Controller name')
    .option('--with-promise <options>', 'With promise (yes) or (no), default is no', 'no')
    .action((args) => {
        let { name, withPromise } = args
        
        if (typeof name == 'function') return console.log('Failed to create new controller')

        name = name.replace('Controller', '')
        name = name.replace('controller', '')
        name = name.replace('.ts', '')
        name = `${name[0].toUpperCase()}${name.slice(1)}Controller`

        withPromise = withPromise === 'yes' ? true : false

        if (fs.existsSync(`./lib/controllers/${name}.ts`)) {
            return console.log('Controller already exists')
        }
    
        const readFileController = fs.readFileSync(`./sample/controllers/${withPromise ? 'SampleController-promise' : 'SampleController'}.txt`).toString()
        const replaceClassName = readFileController.replace(/SampleController/gi, name)
        
        fs.writeFile(`./lib/controllers/${name}.ts`, replaceClassName, (err) => {
            if (err) throw new Error(error)
        
            console.log(`Success to create new controller with class name ${name}`)
        })

        fs.appendFile('./lib/controllers/index.ts', `\r\nexport { ${name} } from './${name}'`, (err) => {
            if (err) throw new Error(err)
        
            console.log('Success to append new controller')
        })
    
        return
    })

program
    .command('make:route')
    .description('Make a new route')
    .option('--name <name>', 'Route name')
    .option('--prefix <prefix>', 'Prefix route')
    .option('--with-promise <options>', 'With promise (yes) or (no), default is no', 'no')
    .action((args) => {
        let { name, prefix, withPromise } = args

        if (typeof name === 'function' || !prefix) return console.log('Failed to create new route')

        name = name.replace('Route', '')
        name = name.replace('route', '')
        name = name.replace('.ts', '')
        name = `${name[0].toUpperCase()}${name.slice(1)}`

        withPromise = withPromise === 'yes' ? true : false

        if (fs.existsSync(`./lib/route/${name}.ts`)) {
            return console.log('Route already exists')
        }
        
        const readFileRoute = fs.readFileSync(`./sample/routes/${withPromise ? 'sample-promise' : 'sample'}.txt`).toString()
        const replaceClassName = readFileRoute.replace(/SampleRoute/gi, `${name}Route`)
        const replacePrefixRoute = replaceClassName.replace(/\/samples/gi, `/${prefix}`)

        const readFile = fs.readFileSync('./lib/routes/index.ts').toString()
        const x = readFile.split('public routes = (app: Application): void => {')
        const y = x[1].split('}')[0]
        const z = x[1].split('}')

        const writeFile = `public routes = (app: Application): void => {${y}    ${name}Route.routes(app)
    }
}`

        const combine = x[0] + writeFile + z[2]
        
        fs.writeFile(`./lib/routes/${name}.ts`, replacePrefixRoute, (err) => {
            if (err) throw new Error(error)
        
            console.log(`Success to create new route with class name ${name}Route`)
        })

        fs.writeFile(`./lib/routes/index.ts`, combine, (err) => {
            if (err) throw new Error(error)
        
            console.log('Success to append index route')

            fs.appendFile('./lib/routes/index.ts', `\r\import ${name}Route from './${name}'`, (err) => {
                if (err) throw new Error(err)
            
                console.log('Success to append new route')
            })
        })
    
        return
    })

program
    .command('append:model')
    .description('Generate append model')
    .option('--name <name>', 'Model name')
    .option('--table <name>', 'Table name')
    .action((args) => {
        let { name, table } = args

        if (typeof name === 'function' || !table) return console.log('Failed to append new model')

        name = `${name[0].toUpperCase()}${name.slice(1)}`

        fs.appendFile('./lib/models/index.ts', `\r\nexport const ${name} = models.${table}`, (err) => {
            if (err) throw new Error(error)
            
            console.log(`Success to append table with name ${name}`)
        })

        return
    })

program.parse(process.argv)
